package scripts.fc.api.skills;

import org.tribot.api.interfaces.Positionable;
import org.tribot.api2007.Player;
import org.tribot.api2007.types.RSArea;

public abstract class GatheringLocation implements Comparable<GatheringLocation>
{	
	protected Positionable centerTile;
	protected RSArea area;
	
	public abstract String getName();
	public abstract Positionable centerTile();
	public abstract boolean goTo();
	public abstract boolean goToBank();
	public abstract boolean hasRequirements();
	public abstract int getRadius();
	
	public GatheringLocation()
	{
		centerTile = centerTile();
		area = getArea();
	}
	
	public RSArea getArea()
	{
		if(area == null && (centerTile != null || centerTile() != null))
			area = new RSArea(centerTile == null ? centerTile() : centerTile, getRadius());
		
		return area;
	}
	
	public Positionable getCenterTile()
	{
		return centerTile;
	}
	
	public int compareTo(GatheringLocation other)
	{
		return Player.getPosition().distanceTo(centerTile.getPosition()) - Player.getPosition().distanceTo(other.getCenterTile().getPosition());
	}
	
	public String toString()
	{
		return getName();
	}
}
