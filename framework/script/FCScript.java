package scripts.fc.framework.script;

import java.awt.Color;
import java.awt.Graphics;
import java.util.Arrays;
import java.util.stream.Stream;

import org.tribot.api.General;
import org.tribot.api.util.ABCUtil;
import org.tribot.api2007.util.ThreadSettings;
import org.tribot.script.Script;
import org.tribot.script.ScriptManifest;
import org.tribot.script.interfaces.Ending;
import org.tribot.script.interfaces.Painting;
import org.tribot.script.interfaces.Starting;

import scripts.fc.api.banking.listening.FCBankObserver;
import scripts.fc.framework.paint.FCPaint;
import scripts.fc.framework.paint.FCPaintable;

public abstract class FCScript extends Script implements FCPaintable, Painting, Starting, Ending
{	
	public final ScriptManifest MANIFEST = (ScriptManifest)this.getClass().getAnnotation(ScriptManifest.class);
	public final FCBankObserver BANK_OBSERVER = new FCBankObserver(this);
	
	protected FCPaint paint = new FCPaint(this, Color.WHITE);
	public ABCUtil abc = new ABCUtil();
	
	protected abstract int mainLogic();
	protected abstract String[] scriptSpecificPaint();
	
	public void run()
	{
		while(true)
		{
			int sleep = mainLogic();
			
			if(sleep == -1)
				return;
			
			sleep(sleep);
		}
	}
	
	public void onStart()
	{
		General.useAntiBanCompliance(true);
		ThreadSettings.get().setClickingAPIUseDynamic(true);
		println("Started " + MANIFEST.name() + " v" + MANIFEST.version() + " by " + MANIFEST.authors()[0]);
	}
	
	public void onEnd()
	{
		println("Thank you for running " + MANIFEST.name() + " v" + MANIFEST.version() + " by " + MANIFEST.authors()[0]);
	}
	
	protected String[] basicPaint()
	{
		return new String[]{MANIFEST.name() + " v" + MANIFEST.version() + " by " + MANIFEST.authors()[0],
				"Time ran: " + paint.getTimeRan()};
	}
	
	public String[] getPaintInfo()
	{
		return Stream.concat(Arrays.stream(basicPaint()), Arrays.stream(scriptSpecificPaint())).toArray(String[]::new);
	}
	
	public void onPaint(Graphics g)
	{
		paint.paint(g);
	}
	
	public FCPaint getPaint()
	{
		return paint;
	}
	
}
