package scripts.fc.missions.fccooksassistant.tasks;

import org.tribot.api.General;
import org.tribot.api.Timing;
import org.tribot.api.interfaces.Positionable;
import org.tribot.api2007.Inventory;
import org.tribot.api2007.Player;
import org.tribot.api2007.WebWalking;
import org.tribot.api2007.types.RSTile;

import scripts.fc.api.generic.FCConditions;
import scripts.fc.api.interaction.impl.objects.ClickObject;
import scripts.fc.api.interaction.impl.objects.ItemOnObject;
import scripts.fc.framework.task.Task;
import scripts.fc.missions.fccooksassistant.data.QuestSettings;

public class MillWheat extends Task
{
	private static final long serialVersionUID = -398207071500798021L;
	
	private final Positionable GROUND_FLOOR_TILE = new RSTile(3165, 3306, 0);
	private final int DISTANCE_THRESHOLD = 3;
	private final int LOADING_ANIM = 3572;
	
	@Override
	public void execute()
	{
		int plane = Player.getPosition().getPlane();
		
		if(plane == 0)
			handleGroundFloor(plane);
		else if(plane == 1)
			climbUp(plane);
		else if(plane == 2)
			mill();
	}

	@Override
	public boolean shouldExecute()
	{
		return QuestSettings.MILL_WHEAT.isValid();
	}

	@Override
	public String getStatus()
	{
		return "Mill wheat";
	}
	
	private void climbUp(int plane)
	{
		if(new ClickObject("Climb-up", "Ladder", 10).execute())
			Timing.waitCondition(FCConditions.planeChanged(plane), 4000);
	}
	
	private void handleGroundFloor(int plane)
	{
		if(Player.getPosition().distanceTo(GROUND_FLOOR_TILE) > DISTANCE_THRESHOLD)
			WebWalking.walkTo(GROUND_FLOOR_TILE);
		else
			climbUp(plane);
	}
	
	private void mill()
	{
		final int INV_SPACE = Inventory.getAll().length;
		
		if(Inventory.getCount("Grain") == 0 || (new ItemOnObject("Use", "Hopper", "Grain", 10).execute() 
				&& Timing.waitCondition(FCConditions.animationChanged(-1), 4000)
				&& Timing.waitCondition(FCConditions.inventoryChanged(INV_SPACE), 5000)
				&& Timing.waitCondition(FCConditions.animationChanged(LOADING_ANIM), 4000)))
		{
			while(!QuestSettings.COLLECT_FLOUR.isValid())
			{
				if(new ClickObject("Operate", "Hopper controls", 10).execute())
					Timing.waitCondition(FCConditions.settingEqualsCondition(695, 1), 5000);
				
				General.sleep(1000);
			}
		}
	}

}
