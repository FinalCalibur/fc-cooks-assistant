package scripts.fc.missions.fccooksassistant.tasks;

import org.tribot.api.Timing;
import org.tribot.api2007.Player;
import org.tribot.api2007.WebWalking;

import scripts.fc.api.generic.FCConditions;
import scripts.fc.api.interaction.impl.grounditems.PickUpGroundItem;
import scripts.fc.framework.task.Task;
import scripts.fc.missions.fccooksassistant.FCCooksAssistant;
import scripts.fc.missions.fccooksassistant.data.QuestSettings;

public class GetPot extends Task
{
	@Override
	public void execute()
	{
		if(!GetBucket.KITCHEN_AREA.contains(Player.getPosition()))
			WebWalking.walkTo(FCCooksAssistant.KITCHEN_TILE);
		else
			if(new PickUpGroundItem("Pot").execute())
				Timing.waitCondition(FCConditions.inventoryContains("Pot"), 7500);
	}

	@Override
	public boolean shouldExecute()
	{
		return QuestSettings.GET_POT.isValid();
	}

	@Override
	public String getStatus()
	{
		return "Get pot";
	}

}
