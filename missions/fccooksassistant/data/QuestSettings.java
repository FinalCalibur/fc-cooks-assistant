package scripts.fc.missions.fccooksassistant.data;

import org.tribot.api2007.Game;

import scripts.fc.framework.quest.QuestBool;
import scripts.fc.missions.fccooksassistant.data.bools.BucketBool;
import scripts.fc.missions.fccooksassistant.data.bools.EggBool;
import scripts.fc.missions.fccooksassistant.data.bools.FlourBool;
import scripts.fc.missions.fccooksassistant.data.bools.MilkBool;
import scripts.fc.missions.fccooksassistant.data.bools.PotBool;
import scripts.fc.missions.fccooksassistant.data.bools.SpaceBool;
import scripts.fc.missions.fccooksassistant.data.bools.WheatBool;

public enum QuestSettings
{
	GET_POT(new int[][]{{29, 0}}, new PotBool(false)),
	GET_BUCKET(new int[][]{{29, 0}}, new BucketBool(false), new PotBool(true)),
	START_QUEST(new int[][]{{29, 0}}, new PotBool(true), new BucketBool(true)),
	MILK_COW(new int[][]{{29, 1}}, new BucketBool(true), new MilkBool(false)),
	GET_EGG(new int[][]{{29, 1}}, new MilkBool(true), new EggBool(false)),
	PICK_WHEAT(new int[][]{{29, 1}, {695, 0}}, new MilkBool(true), new EggBool(true), new WheatBool(false), new FlourBool(false)),
	MILL_WHEAT(new int[][]{{29, 1}, {695, 0}}, new MilkBool(true), new EggBool(true), new WheatBool(true)),
	COLLECT_FLOUR(new int[][]{{29, 1}, {695, 1}}, new MilkBool(true), new EggBool(true)),
	TURN_IN_QUEST(new int[][]{{29, 1}, {695, 0}}, new MilkBool(true), new EggBool(true), new FlourBool(true)),
	QUEST_COMPLETE(new int[][]{{29, 2}});
	
	private int[][] settings;
	private QuestBool[] bools;
	public final static SpaceBool SPACE_BOOL = new SpaceBool(true);
	
	QuestSettings(int[][] settings, QuestBool... bools)
	{
		this.settings = settings;
		this.bools = bools;
	}
	
	public boolean isValid()
	{
		if(!SPACE_BOOL.validate())
			return false;
		
		for(int i = 0; i < settings.length; i++)
		{
			if(Game.getSetting(settings[i][0]) != settings[i][1])
				return false;
		}
		
		for(QuestBool b : bools)
		{
			if(!b.validate())
				return false;
		}
		
		return true;
	}
}
